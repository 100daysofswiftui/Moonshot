//
//  MoonshotApp.swift
//  Moonshot
//
//  Created by Pascal Hintze on 07.11.2023.
//

import SwiftUI

@main
struct MoonshotApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
