//
//  Astronaut.swift
//  Moonshot
//
//  Created by Pascal Hintze on 07.11.2023.
//

import Foundation

struct Astronaut: Codable, Identifiable, Hashable {
    let id: String
    let name: String
    let description: String
}
