# Moonshot

Moonshot is an app that lets users learn about the missions and astronauts that formed NASA’s Apollo space program

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/moonshot-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- ScrollView
- NavigationLink
- Codable
- Container relative frames
- ScrollView